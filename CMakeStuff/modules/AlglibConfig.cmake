#sudo apt install libalglib-dev
 
# Look for the necessary header
set(Alglib_INCLUDE_DIR /usr/include/libalglib)
mark_as_advanced(Alglib_INCLUDE_DIR)

# Look for the necessary library
set(Alglib_LIBRARY /usr/lib/x86_64-linux-gnu/libalglib.so)
mark_as_advanced(Alglib_LIBRARY)

set(Alglib_FOUND 1)


    set(Alglib_INCLUDE_DIRS ${Alglib_INCLUDE_DIR})
    set(Alglib_LIBRARIES ${Alglib_LIBRARY})
    if(NOT TARGET Alglib::Alglib)
        add_library(Alglib::Alglib UNKNOWN IMPORTED)
        set_target_properties(Alglib::Alglib PROPERTIES
            IMPORTED_LOCATION             "${Alglib_LIBRARY}"
            INTERFACE_INCLUDE_DIRECTORIES "${Alglib_INCLUDE_DIR}")
    endif()
