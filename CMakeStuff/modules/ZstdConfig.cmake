#sudo apt install libzstd-dev
 
# Look for the necessary header
set(Zstd_INCLUDE_DIR /usr/include)
mark_as_advanced(Zstd_INCLUDE_DIR)

# Look for the necessary library
set(Zstd_LIBRARY /usr/lib/x86_64-linux-gnu/libzstd.so)
mark_as_advanced(Zstd_LIBRARY)

set(Zstd_FOUND 1)


    set(Zstd_INCLUDE_DIRS ${Zstd_INCLUDE_DIR})
    set(Zstd_LIBRARIES ${Zstd_LIBRARY})
    if(NOT TARGET Zstd::Zstd)
        add_library(Zstd::Zstd UNKNOWN IMPORTED)
        set_target_properties(Zstd::Zstd PROPERTIES
            IMPORTED_LOCATION             "${Zstd_LIBRARY}"
            INTERFACE_INCLUDE_DIRECTORIES "${Zstd_INCLUDE_DIR}")
    endif()
