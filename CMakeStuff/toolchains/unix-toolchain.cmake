message("UNIX non APPLE environment")
message("Please run the configuration like this:")
message("cmake -G \"Unix Makefiles\" -DCMAKE_BUILD_TYPE=Debug ../development")
message("If using the locally built pappsomspp libs, add -DLOCAL_DEV=1")

set(CMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES /usr/include)
set(CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES /usr/include)

find_package( Qt6 COMPONENTS Core Gui Svg Xml PrintSupport Sql Network Concurrent Widgets REQUIRED )


find_package(QCustomPlotQt6 REQUIRED)


find_package(OdsStream REQUIRED)


find_package(PappsoMSpp COMPONENTS Core Widget REQUIRED)



find_package(QuaZip-Qt6 REQUIRED)

find_package(ZLIB REQUIRED)

