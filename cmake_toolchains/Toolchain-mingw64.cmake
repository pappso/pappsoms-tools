
# PAPPSOms tools
# cd buildwin64
# cmake -DCMAKE_C_COMPILER="/mingw64/bin/x86_64-w64-mingw32-gcc.exe" -DCMAKE_CXX_COMPILER="/mingw64/bin/x86_64-w64-mingw32-g++.exe" -G"Unix Makefiles" -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE=../cmake_toolchains/Toolchain-mingw64.cmake ..

#export CC=/mingw64/bin/x86_64-w64-mingw32-g++.exe
#export CXX=/mingw64/bin/x86_64-w64-mingw32-gcc.exe
#set(CMAKE_SYSTEM_NAME Linux)

#VERBOSE=1 make
SET (USEQT5 1)
set (MINGW 1)
set (MAKE_TEST 1)

#set(CMAKE_CXX_COMPILER "/mingw64/bin/x86_64-w64-mingw32-g++.exe")
#set(CMAKE_C_COMPILER "/mingw64/bin/x86_64-w64-mingw32-gcc.exe")

set (QCustomPlot_FOUND TRUE)
set (QCustomPlot_INCLUDES "c:/msys64/home/polipo/devel/qcustomplot_2.0.1+dfsg1-1")
set (QCustomPlot_LIBRARIES "c:/msys64/home/polipo/devel/qcustomplot_2.0.1+dfsg1-1/build/libqcustomplot.dll")


set(PAPPSOMSPP_QT5_FOUND 1)
set(PAPPSOMSPP_WIDGET_QT5_FOUND 1)
set(PAPPSOMSPP_INCLUDE_DIR "c:/msys64/home/polipo/devel/pappsomspp/src")
set(PAPPSOMSPP_QT5_LIBRARY "c:/msys64/home/polipo/devel/pappsomspp/build/src/libpappsomspp-qt5.a")
set(PAPPSOMSPP_WIDGET_QT5_LIBRARY "c:/msys64/home/polipo/devel/pappsomspp/build/src/pappsomspp/widget/libpappsomspp-widget-qt5.a")

if(NOT TARGET Pappso::Core)
        add_library(Pappso::Core UNKNOWN IMPORTED)
        set_target_properties(Pappso::Core PROPERTIES
            IMPORTED_LOCATION             "${PAPPSOMSPP_QT5_LIBRARY}"
            INTERFACE_INCLUDE_DIRECTORIES "${PAPPSOMSPP_INCLUDE_DIR}")
endif()


if(NOT TARGET Pappso::Widget)
        add_library(Pappso::Widget UNKNOWN IMPORTED)
        set_target_properties(Pappso::Widget PROPERTIES
            IMPORTED_LOCATION             "${PAPPSOMSPP_WIDGET_QT5_LIBRARY}"
            INTERFACE_INCLUDE_DIRECTORIES "${PAPPSOMSPP_INCLUDE_DIR}")
endif()

#produce shared library as DLL file
set(
    CMAKE_SHARED_LIBRARY_CXX_FLAGS
    "${CMAKE_SHARED_LIBRARY_CXX_FLAGS} -shared -Wl,--subsystem,windows -DDLL"
)




set (QUAZIP_FOUND TRUE)
# QUAZIP_INCLUDE_DIR         - Path to QuaZip include dir
set (QUAZIP_INCLUDE_DIR "c:/msys64/home/polipo/devel/quazip-0.7.6")
# QUAZIP_INCLUDE_DIRS        - Path to QuaZip and zlib include dir (combined from QUAZIP_INCLUDE_DIR + ZLIB_INCLUDE_DIR)
# QUAZIP_LIBRARIES           - List of QuaZip libraries
set (QUAZIP_QT5_LIBRARIES "c:/msys64/home/polipo/devel/quazip-0.7.6/build/libquazip5.dll")
# QUAZIP_ZLIB_INCLUDE_DIR    - The include dir of zlib headers


set(ODSSTREAM_QT5_FOUND 1)
set(ODSSTREAM_INCLUDE_DIR "c:/msys64/home/polipo/devel/libodsstream/src")
set(ODSSTREAM_QT5_LIBRARY "c:/msys64/home/polipo/devel/libodsstream/build/src/libodsstream-qt5.dll")
if(NOT TARGET OdsStream::Core)
    add_library(OdsStream::Core UNKNOWN IMPORTED)
    set_target_properties(OdsStream::Core PROPERTIES
            IMPORTED_LOCATION             "${ODSSTREAM_QT5_LIBRARY}"
            INTERFACE_INCLUDE_DIRECTORIES "${ODSSTREAM_INCLUDE_DIR}"
    )
endif()


