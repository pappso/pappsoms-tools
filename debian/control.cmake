Source: pappsoms-tools
Maintainer: Olivier Langella <olivier.langella@u-psud.fr>
Homepage: http://pappso.inra.fr/bioinfo
Section: libs
Priority: optional
Build-Depends: debhelper (>= 9),
               cmake (>= 2.6),
               qt6-base-dev,
               libpappsomspp-widget-dev (>= 0.9.25),
               libodsstream-dev,
               libqcustomplot-dev (>= 2.1.0),
               libxkbcommon-x11-dev,
               libboost-container-dev,
               qt6-svg-dev
Standards-Version: 3.9.4

Package: pappsoms-tools
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         libpappsomspp-widget0 (>= @LIBPAPPSOMSPP_VERSION@),
         libodsstream0
Pre-Depends: ${misc:Pre-Depends}
Description: set of proteomic tools used on PAPPSO. computes peptide mass, isotope ratio,
 fragmentation products, protein grouper.
