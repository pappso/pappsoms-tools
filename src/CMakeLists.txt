# CMake script for PAPPSOms++ library
# Author: Olivier Langella
# Created: 03/03/2015

# Appeler automatique le moc quand nécessaire
#SET(CMAKE_AUTOMOC ON)
# Les fichiers générés par le moc sont générés dans le dossier bin, dire à CMake de toujours 	
# inclure les entêtes de ce dossier
#SET(CMAKE_INCLUDE_CURRENT_DIR ON)

#FIND_PACKAGE(Boost REQUIRED)
#INCLUDE_DIRECTORIES( ${Boost_INCLUDE_DIRS})




SET(pappsomstools_RCCS "../pappsoms-tools.qrc")
  
qt6_add_resources(pappsomstools_RCC_SRCS ${pappsomstools_RCCS})



if(PappsoMSpp_FOUND)
else(PappsoMSpp_FOUND)
  message("PAPPSOms++ library not found")
  message("did you apt-get install libpappsomspp-dev ?")
endif(PappsoMSpp_FOUND)


if(PappsoMSppWidget_FOUND)
else(PappsoMSppWidget_FOUND)
  message("PAPPSOms++ widget library not found")
  message("did you apt-get install libpappsomspp-widget-dev ?")
endif(PappsoMSppWidget_FOUND)

message("PAPPSOms++ library : ${PappsoMSpp_LIBRARIES}")

#sudo apt-get install libpappsomspp-dev


configure_file (${CMAKE_SOURCE_DIR}/src/config.h.cmake ${CMAKE_SOURCE_DIR}/src/config.h)


SET(H_FILES
	utils/consoleout.h
	utils/pappsomstoolsexception.h
)

add_executable(pt-peptidemass pt_peptidemass.cpp ${CPP_FILES} )
target_link_libraries(pt-peptidemass
    Qt6::Gui
    OdsStream::Core
	QuaZip::QuaZip
    PappsoMSpp::Core
)


ADD_EXECUTABLE(pt-fastagrouper pt_fastagrouper.cpp ${CPP_FILES} )
target_link_libraries(pt-fastagrouper
    PappsoMSpp::Core
    Qt6::Gui
    OdsStream::Core
	QuaZip::QuaZip
)


ADD_EXECUTABLE(pt-fastadigestor ptfastadigestor.cpp )

TARGET_LINK_LIBRARIES(pt-fastadigestor
    PappsoMSpp::Core
    Qt6::Gui
    OdsStream::Core
	QuaZip::QuaZip
)



ADD_EXECUTABLE(pt-fastarenamer ptfastarenamer.cpp utils/mapods.cpp )
target_link_libraries(pt-fastarenamer
    PappsoMSpp::Core
    Qt6::Gui
    OdsStream::Core
	QuaZip::QuaZip
)





add_executable(pt-peptideions ptpeptideions.cpp )
target_link_libraries(pt-peptideions 
    Qt6::Core 
    Qt6::Gui 
    Qt6::Svg 
    Qt6::PrintSupport 
    PappsoMSpp::Core
    PappsoMSpp::Widget
    OdsStream::Core
	QuaZip::QuaZip
    QCustomPlotQt6::QCustomPlotQt6
)



add_executable(pt-psimodsearch ptpsimodsearch.cpp ${CPP_FILES} )

target_link_libraries(pt-psimodsearch
	Qt6::Gui
	PappsoMSpp::Core
	OdsStream::Core
	QuaZip::QuaZip
)







ADD_EXECUTABLE(pt-fastatrypticpeptidecount ptfastatrypticpeptidecount.cpp)

TARGET_LINK_LIBRARIES(pt-fastatrypticpeptidecount 
	Qt6::Gui
	PappsoMSpp::Core
	OdsStream::Core
	QuaZip::QuaZip
)






ADD_EXECUTABLE(pt-mzxmlconverter ptmzxmlconverter.cpp)

TARGET_LINK_LIBRARIES(pt-mzxmlconverter Qt6::Core Qt6::Sql PappsoMSpp::Core)




ADD_EXECUTABLE(pt-spectrumdecoder ptspetrumdecoder.cpp)

TARGET_LINK_LIBRARIES(pt-spectrumdecoder Qt6::Core Qt6::Sql PappsoMSpp::Core)




ADD_EXECUTABLE(pt-spectrumproteinmatcher ptspectrumproteinmatcher.cpp lib/proteincollection.cpp lib/spectrumintegercode.cpp)

TARGET_LINK_LIBRARIES(pt-spectrumproteinmatcher Qt6::Core Qt6::Sql PappsoMSpp::Core)

add_subdirectory(gui)




