
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#ifndef TANDEMPARAM_H
#define TANDEMPARAM_H

#include <QMainWindow>
#include <pappsomspp/psm/xtandem/xtandemspectrumprocess.h>
#include <QCloseEvent>

// test :
// /gorgone/pappso/formation/Janvier2014/TD/mzXML/20120906_balliau_extract_1_A01_urnb-1.mzXML
// scan 9325
// KGGFADEGSATTESDIEETLKR
// xtandem peptide hyperscore 	67.200


using namespace pappso;

namespace Ui
{
  class TandemParam;
}

using namespace pappso;
class TandemParam : public QMainWindow
{
  Q_OBJECT

  public:
  explicit TandemParam(QWidget *parent = 0);
  ~TandemParam();

  void closeEvent(QCloseEvent *event);

  XtandemSpectrumProcess getXtandemSpectrumProcess() const;

  public slots:
  void removeIsotopeBoxClicked(bool isChecked);
  void minimumMzSpinBoxValueChanged(double min_mz);
  void dynamicRangeSpinBoxValueChanged(double dynamic_range);
  void nmostIntenseSpinBoxValueChanged(int nmost_intense);
  void excludeParentIonChecked(bool isChecked);
  void excludeNeutralLossChecked(bool isChecked);
  void neutralLossMassValueChanged(double value);
  void neutralLossWindowDaltonValueChanged(double value);
  void refineSpectrumModelChecked(bool isChecked);
  void yIonChecked(bool isChecked);
  void yoIonChecked(bool isChecked);

  void bIonChecked(bool isChecked);
  void boIonChecked(bool isChecked);

  void ystarIonChecked(bool isChecked);

  void bstarIonChecked(bool isChecked);

  void cIonChecked(bool isChecked);

  void zIonChecked(bool isChecked);

  void aIonChecked(bool isChecked);
  void xIonChecked(bool isChecked);


  signals:
  void xtandemSpectrumProcessChanged(XtandemSpectrumProcess spectrum_process);

  private:
  Ui::TandemParam *ui;

  XtandemSpectrumProcess _spectrum_process;
};

#endif // TANDEMPARAM_H
