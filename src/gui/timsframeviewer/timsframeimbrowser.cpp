
/*******************************************************************************
 * Copyright (c) 2022 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


#include "timsframeimbrowser.h"

TimsFrameImBrowser::TimsFrameImBrowser(
  pappso::TimsFrameBaseCstSPtr tims_frame_sptr)
{
  mcsp_timsFrame = tims_frame_sptr;

  computeTotalSpectrum();
}

TimsFrameImBrowser::~TimsFrameImBrowser()
{
}


void
TimsFrameImBrowser::computeTotalSpectrum()
{
  pappso::TimsDataFastMap raw_spectrum =
    pappso::TimsDataFastMap::getTimsDataFastMapInstance();
  mcsp_timsFrame.get()->combineScansInTofIndexIntensityMap(
    raw_spectrum, 0, mcsp_timsFrame.get()->getTotalNumberOfScans() - 1);


  for(quint32 tof_index : raw_spectrum.getTofIndexList())
    {
      m_totalSpectrum.push_back(
        {(double)tof_index, (double)raw_spectrum.readIntensity(tof_index)});
    }
  m_totalSpectrum.sortX();
}

std::vector<pappso::TraceCstSPtr>
TimsFrameImBrowser::getImTraces(std::size_t max_number)
{
  std::vector<pappso::TraceCstSPtr> traces;


  m_totalSpectrum.sortY();

  for(auto &datapoint : m_totalSpectrum)
    {
      pappso::Trace trace(
        mcsp_timsFrame.get()->getIonMobilityTraceByTofIndexRange(
          datapoint.x - 1,
          datapoint.x + 1,
          pappso::XicExtractMethod::sum,
          0,
          2000));

      traces.push_back(trace.makeTraceSPtr());
      if(traces.size() >= max_number)
        break;
    }

  return traces;
}
