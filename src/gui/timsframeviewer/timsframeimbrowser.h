
/*******************************************************************************
 * Copyright (c) 2022 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <pappsomspp/vendors/tims/timsframebase.h>
/**
 * @todo write docs
 */
class TimsFrameImBrowser
{
  public:
  /**
   * Default constructor
   */
  TimsFrameImBrowser(pappso::TimsFrameBaseCstSPtr tims_frame_sptr);

  /**
   * Destructor
   */
  virtual ~TimsFrameImBrowser();


  std::vector<pappso::TraceCstSPtr> getImTraces(std::size_t max_number);

  private:
  void computeTotalSpectrum();

  private:
  pappso::TimsFrameBaseCstSPtr mcsp_timsFrame;


  pappso::Trace m_totalSpectrum;
};
