
/*******************************************************************************
 * Copyright (c) 2022 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@universite-paris-saclay.fr> - initial
 *API and implementation
 ******************************************************************************/

#include "timsframewindow.h"
#include "ui_timsframewindow.h"
#include <pappsomspp/msrun/private/timsmsrunreaderms2.h>
#include <pappsomspp/xic/xic.h>
#include <pappsomspp/pappsoexception.h>
#include <QDebug>
#include "timsframeimbrowser.h"

TimsFrameWindow::TimsFrameWindow(QWidget *parent)
  : QMainWindow(parent), ui(new Ui::TimsFrameWindow)
{
  ui->setupUi(this);

  ui->timsframe_widget->setAxisLabelX("im");
  ui->timsframe_widget->setAxisLabelY("mz");
  mp_timsMsRunReaderMs2 = nullptr;
}

TimsFrameWindow::~TimsFrameWindow()
{
}

void
TimsFrameWindow::handleMsDataFile(pappso::MsRunReaderSPtr p_ms_data_file)
{
  qDebug();

  msp_msRunReader = p_ms_data_file;
  qDebug() << p_ms_data_file.get()->getMsRunId().get()->getSampleName();
  ui->statusbar->showMessage(
    p_ms_data_file.get()->getMsRunId().get()->getSampleName());

  if(p_ms_data_file.get()->getMsRunId().get()->getMsDataFormat() ==
     pappso::MsDataFormat::brukerTims)
    {

      mp_timsMsRunReaderMs2 =
        dynamic_cast<pappso::TimsMsRunReaderMs2 *>(p_ms_data_file.get());
      if(mp_timsMsRunReaderMs2 != nullptr)
        {
          qDebug();
          mp_timsMsRunReaderMs2->setMs2BuiltinCentroid(true);
          /*
                    std::shared_ptr<pappso::FilterSuiteString> ms2filter;
                    QString filters_str =
                      "chargeDeconvolution|0.02dalton mzExclusion|0.01dalton";
                    ms2filter =
             std::make_shared<pappso::FilterSuiteString>(filters_str);


                    tims2_reader->setMs2FilterCstSPtr(ms2filter);
          */
          qDebug();
        }
    }
  else
    {
      mp_timsMsRunReaderMs2 = nullptr;
    }
}

void
TimsFrameWindow::frameNumberChanged(int frame_number)
{
  qDebug() << frame_number;
  if(mp_timsMsRunReaderMs2 != nullptr)
    {
      pappso::TimsFrameCstSPtr timsframe_sptr;
      try
        {
          timsframe_sptr =
            mp_timsMsRunReaderMs2->getTimsDataSPtr().get()->getTimsFrameCstSPtr(
              frame_number);
        }
      catch(pappso::PappsoException &exception)
        {
          qDebug() << exception.qwhat();
          // throw exception;
          QMessageBox::warning(
            this,
            tr("Oops! an error occurred in peptide viwer. Don't panic :"),
            exception.qwhat());
          return;
        }

      pappso::ColorMapPlotConfig color_map_plot_config;
      color_map_plot_config.keyCellCount =
        timsframe_sptr.get()->getTotalNumberOfScans();
      color_map_plot_config.maxKeyValue =
        timsframe_sptr.get()->getTotalNumberOfScans() - 1;
      color_map_plot_config.minKeyValue = 0;
      color_map_plot_config.minMzValue  = 100000;
      color_map_plot_config.maxMzValue =
        timsframe_sptr.get()->getMaximumRawMassIndex();
      color_map_plot_config.mzCellCount =
        color_map_plot_config.maxMzValue - color_map_plot_config.minMzValue;
      color_map_plot_config.setOrigAndLastMaxZValue(300);
      color_map_plot_config.setOrigAndLastMinZValue(0);

      QColor color("black");

      qDebug() << timsframe_sptr.get()->getId();
      qDebug();
      if(mp_colorMap != nullptr)
        {
          ui->timsframe_widget->removePlottable(mp_colorMap);
        }
      mp_colorMap = ui->timsframe_widget->addColorMap(
        *timsframe_sptr.get(), color_map_plot_config, color);

      qDebug() << mp_colorMap->data()->keyRange();
      qDebug() << mp_colorMap->data()->valueRange();
      qDebug() << mp_colorMap->data()->dataBounds();

      qDebug() << mp_colorMap->data()->keySize();
      qDebug() << mp_colorMap->data()->valueSize();
      /*
for(int key = 0; key < mp_colorMap->data()->keySize(); key++)
{

  for(int ivalue = 0; ivalue < mp_colorMap->data()->valueSize();
      ivalue++)
    {
      double z = mp_colorMap->data()->cell(key, ivalue);
      if(z > 0)
        {
          qDebug() << "key=" << key << " value=" << ivalue << " data=" << z;
        }
    }
}*/

      ui->timsframe_widget->zAxisFilterLowPassThreshold(300);
      // ui->timsframe_widget->zAxisScaleToLog10();
      ui->timsframe_widget->replot();


      TimsFrameImBrowser im_browser(timsframe_sptr);


      std::vector<pappso::TraceCstSPtr> trace_list =
        im_browser.getImTraces(1000);

      qDebug();

      for(auto trace : trace_list)
        {
          pappso::Xic xic(*trace.get());
          m_tracePlotList.push_back(
            ui->imTraceWidget->addXicSp(xic.makeXicCstSPtr()));

          m_tracePlotList.back()->setVisible(true);
        }
      imIonChanged(0);
      // ui->massSpectrumWidget->rescale();
      ui->imTraceWidget->rescale();
      qDebug();
    }

  qDebug();
}

void
TimsFrameWindow::imIonChanged(int ion_number)
{
  if((std::size_t)ion_number < m_tracePlotList.size())
    {
      m_tracePlotList[m_currentImIon]->setVisible(false);
      m_currentImIon = ion_number;
      m_tracePlotList[m_currentImIon]->setVisible(true);
      ui->imTraceWidget->plot();
      ui->imTraceWidget->rescale();
    }
}
