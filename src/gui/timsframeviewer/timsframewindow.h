
/*******************************************************************************
 * Copyright (c) 2022 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@universite-paris-saclay.fr> - initial
 *API and implementation
 ******************************************************************************/
#pragma once


#include <QMainWindow>
#include <QThread>
#include <QFileInfo>
#include <QMutex>
#include <QMessageBox>
#include <pappsomspp/types.h>
#include <pappsomspp/msfile/msfileaccessor.h>
#include "ui_timsframewindow.h"


namespace Ui
{
class TimsFrameWindow;
}

class TimsFrameWindow : public QMainWindow
{
  Q_OBJECT
  QThread m_workerThread;

  public:
  explicit TimsFrameWindow(QWidget *parent = 0);
  virtual ~TimsFrameWindow();


  public slots:
  void handleMsDataFile(pappso::MsRunReaderSPtr p_ms_data_file);
  void frameNumberChanged(int frame_number);
  void imIonChanged(int ion_number);

  private:
  Ui::TimsFrameWindow *ui;
  pappso::MsRunReaderSPtr msp_msRunReader;
  pappso::TimsMsRunReaderMs2 *mp_timsMsRunReaderMs2;
  QCPColorMap *mp_colorMap = nullptr;
  std::vector<QCPGraph *> m_tracePlotList;
  std::size_t m_currentImIon = 0;
};
