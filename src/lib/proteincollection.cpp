
/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "proteincollection.h"

#include <pappsomspp/protein/proteinpresenceabsencematrix.h>

ProteinCollection::ProteinCollection()
{


  m_aaCode.addAaModification('C',
                             pappso::AaModification::getInstance("MOD:00397"));
  msp_aaCodec = std::make_shared<pappso::AaStringCodec>(m_aaCode);
}

ProteinCollection::ProteinCollection(const ProteinCollection &other)
{
}

ProteinCollection::~ProteinCollection()
{
}

void
ProteinCollection::setSequence(const QString &description_in,
                               const QString &sequence_in)
{


  pappso::ProteinSp protein_sp =
    pappso::Protein(description_in, sequence_in).makeProteinSp();

  qDebug() << protein_sp.get()->getAccession();
  // pappso::ProteinIntegerCode protein_code(protein_sp, aa_codec, 5);

  m_codedProteinArray.push_back({protein_sp, m_aaCode, 5});
}

const pappso::AaCode &
ProteinCollection::getAaCode() const
{
  return m_aaCode;
}


void
ProteinCollection::evaluateSpectrumIntegerCode(
  const SpectrumIntegerCode &coded_spectrum) const
{

  std::vector<double> convolution_score;
  pappso::ProteinPresenceAbsenceMatrix presence_absence_matrix;
  pappso::ProteinPresenceAbsenceMatrix best_presence_absence_matrix;

  double max_score = 0;
  pappso::ProteinSp max_protein;

  for(auto &coded_protein : m_codedProteinArray)
    {
      pappso::ProteinSp protein_sp = coded_protein.getProteinSp();

      presence_absence_matrix.fillMatrix(
        coded_protein, coded_spectrum.getShortPeptideCodeList());

      // qDebug() << protein_sp.get()->getAccession();
      std::vector<double> vec_score = presence_absence_matrix.convolution();

      auto it_score = std::max_element(vec_score.begin(), vec_score.end());

      // double score = std::accumulate(it_score - 2, it_score + 5, 0);

      if(it_score != vec_score.end())
        {
          // std::cout << protein_sp.get()->getAccession().toStdString() << "
          // "
          //         << score << std::endl;
          double score = std::accumulate(it_score - 2, it_score + 5, 0);

          if(score > max_score)
            {
              max_score                    = score;
              max_protein                  = protein_sp;
              convolution_score            = vec_score;
              best_presence_absence_matrix = presence_absence_matrix;
            }
        }
    }


  std::cout << coded_spectrum.getQualifiedMassSpectrum()
                 .getMassSpectrumId()
                 .getSpectrumIndex()
            << " " << max_protein.get()->getAccession().toStdString() << " "
            << max_score << std::endl;
}
