
/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once


#include <pappsomspp/amino_acid/aastringcodec.h>
#include <pappsomspp/fasta/fastahandlerinterface.h>
#include <pappsomspp/protein/proteinintegercode.h>

#include "spectrumintegercode.h"

/**
 * @todo write docs
 */
class ProteinCollection : public pappso::FastaHandlerInterface
{
  public:
  /**
   * Default constructor
   */
  ProteinCollection();

  /**
   * Copy constructor
   *
   * @param other TODO
   */
  ProteinCollection(const ProteinCollection &other);

  /**
   * Destructor
   */
  ~ProteinCollection();


  void setSequence(const QString &description_in,
                   const QString &sequence_in) override;

  void
  evaluateSpectrumIntegerCode(const SpectrumIntegerCode &coded_spectrum) const;

  const pappso::AaCode &getAaCode() const;

  private:
  pappso::AaCode m_aaCode;
  pappso::AaStringCodecSp msp_aaCodec;

  std::vector<pappso::ProteinIntegerCode> m_codedProteinArray;
};
