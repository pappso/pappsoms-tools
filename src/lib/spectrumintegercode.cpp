/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


#include <pappsomspp/processing/filters/filterresample.h>
#include <pappsomspp/processing/filters/filterpass.h>
#include <pappsomspp/processing/filters/filterchargedeconvolution.h>
#include <pappsomspp/processing/filters/filterpeakdelta.h>

#include "spectrumintegercode.h"

SpectrumIntegerCode::SpectrumIntegerCode(
  const pappso::QualifiedMassSpectrum &mass_spectrum)
  : m_originalMassSpectrum(mass_spectrum)
{
  pappso::MassSpectrum spectrum(
    *(m_originalMassSpectrum.getMassSpectrumSPtr().get()));
  pappso::FilterResampleKeepGreater(160).filter(spectrum);
  pappso::FilterChargeDeconvolution(
    pappso::PrecisionFactory::getDaltonInstance(0.02))
    .filter(spectrum);
  //.applyCutOff(150).takeNmostIntense(100).applyDynamicRange(100);
  pappso::FilterGreatestY(100).filter(spectrum);

  pappso::FilterPeakDelta filter_peak_delta;
  filter_peak_delta.filter(spectrum);

  pappso::FilterGreatestY(200).filter(spectrum);
  m_massDeltaList = spectrum.xValues();
}

SpectrumIntegerCode::~SpectrumIntegerCode()
{
}


void
SpectrumIntegerCode::aminoAcidMassMatching(
  const pappso::AaStringCodeMassMatching &aa_matching)
{


  m_shortPeptideCodeList = aa_matching.getAaCodeFromMassList(m_massDeltaList);


  std::sort(m_shortPeptideCodeList.begin(), m_shortPeptideCodeList.end());
  m_shortPeptideCodeList.erase(
    std::unique(m_shortPeptideCodeList.begin(), m_shortPeptideCodeList.end()),
    m_shortPeptideCodeList.end());
}

const std::vector<uint32_t> &
SpectrumIntegerCode::getShortPeptideCodeList() const
{
  return m_shortPeptideCodeList;
}

const pappso::QualifiedMassSpectrum &
SpectrumIntegerCode::getQualifiedMassSpectrum() const
{
  return m_originalMassSpectrum;
}
