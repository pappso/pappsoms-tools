/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <pappsomspp/massspectrum/qualifiedmassspectrum.h>
#include <pappsomspp/amino_acid/aastringcodemassmatching.h>

/**
 * @todo write docs
 */
class SpectrumIntegerCode
{

  public:
  SpectrumIntegerCode(const pappso::QualifiedMassSpectrum &mass_spectrum);

  virtual ~SpectrumIntegerCode();

  void
  aminoAcidMassMatching(const pappso::AaStringCodeMassMatching &aa_matching);

  const std::vector<uint32_t> &getShortPeptideCodeList() const;

  const pappso::QualifiedMassSpectrum &getQualifiedMassSpectrum() const;

  private:
  const pappso::QualifiedMassSpectrum m_originalMassSpectrum;

  std::vector<double> m_massDeltaList;

  std::vector<uint32_t> m_shortPeptideCodeList;
};
