
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once


#include <QDebug>

#include <pappsomspp/peptide/peptidefragmentionlistbase.h>
#include <odsstream/calcwriterinterface.h>
#include <pappsomspp/peptide/peptide.h>
#include <QObject>
#include <QApplication>
#include "config.h"

#include <pappsomspp/massspectrum/massspectrum.h>


class PtPeptideIons : public QObject
{
  Q_OBJECT

  private:
  QCoreApplication *app;
  void writePeptideIonTable(CalcWriterInterface &writer,
                            const pappso::PeptideSp &peptideSp,
                            const pappso::MassSpectrumSPtr &spectrumSp,
                            unsigned int zmax,
                            pappso::PrecisionPtr precision,
                            std::list<pappso::PeptideIon> ion_list);
  void saveSvg(const QString &filename,
               const pappso::PeptideSp &peptideSp,
               const pappso::MassSpectrumSPtr &spectrumSp,
               unsigned int zmax,
               pappso::PrecisionPtr precision,
               std::list<pappso::PeptideIon> ion_list);

  public:
  void windaube_exit();

  explicit PtPeptideIons(QObject *parent = 0);
  /////////////////////////////////////////////////////////////
  /// Call this to quit application
  /////////////////////////////////////////////////////////////
  void quit();

  signals:
  /////////////////////////////////////////////////////////////
  /// Signal to finish, this is connected to Application Quit
  /////////////////////////////////////////////////////////////
  void finished();

  public slots:
  /////////////////////////////////////////////////////////////
  /// This is the slot that gets called from main to start everything
  /// but, everthing is set up in the Constructor
  /////////////////////////////////////////////////////////////
  void run();

  /////////////////////////////////////////////////////////////
  /// slot that get signal when that application is about to quit
  /////////////////////////////////////////////////////////////
  void aboutToQuitApp();
};
