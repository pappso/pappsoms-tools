/**
 * \file ptspectrumproteinmatcher.h
 * \date 14/10/2023
 * \author Olivier Langella
 * \brief match spectrum to protein sequences
 */


/*******************************************************************************
 * Copyright (c) 2023 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "ptspectrumproteinmatcher.h"

#include <QDebug>
#include <QCommandLineParser>
#include <QDateTime>
#include <QTimer>
#include <QFile>
#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/msfile/msfileaccessor.h>
#include <pappsomspp/msrun/private/timsmsrunreaderms2.h>
#include <pappsomspp/processing/uimonitor/uimonitorvoid.h>
#include <pappsomspp/processing/uimonitor/uimonitortextpercent.h>
#include <pappsomspp/processing/filters/filtersuitestring.h>
#include <pappsomspp/processing/filters/filterpeakdelta.h>
#include <pappsomspp/amino_acid/aastringcodemassmatching.h>
#include <pappsomspp/msfile/mzformatenumstr.h>
#include <pappsomspp/amino_acid/aacode.h>
#include <pappsomspp/amino_acid/aamodification.h>
#include <pappsomspp/processing/filters/filterresample.h>
#include <pappsomspp/processing/filters/filterpass.h>

#include <pappsomspp/processing/filters/filterchargedeconvolution.h>
#include <pappsomspp/fasta/fastareader.h>
#include <pappsomspp/protein/protein.h>
#include <pappsomspp/processing/uimonitor/uimonitortext.h>
#include <QFileInfo>
#include <QThreadPool>
#include "lib/proteincollection.h"
#include "lib/spectrumintegercode.h"


//./src/pt-spetrumproteinmatcher -i
/// gorgone/pappso/data_extraction_pappso/mzML/20120906_balliau_extract_1_A10_urzb-3.mzML
//-f fastafile
PtSpectrumProteinMatcher::PtSpectrumProteinMatcher(QObject *parent
                                                   [[maybe_unused]])
{

  // get the instance of the main application
  app = QCoreApplication::instance();
  // setup everything here
  // create any global objects
  // setup debug and warning mode
}


void
PtSpectrumProteinMatcher::run()
{
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));
  /* ./src/pt-mzxmlconverter -m --ms2filters "chargeDeconvolution|0.02dalton" -i
   /gorgone/pappso/fichiers_fabricants/Bruker/tims_doc/tdf-sdk/example_data/200ngHeLaPASEF_2min_compressed.d/analysis.tdf
   -o
   /gorgone/pappso/versions_logiciels_pappso/xtpcpp/bruker/200ngHeLaPASEF_2min.mzXML
  */

  //./src/pt-mzxmlconverter -i
  /// gorgone/pappso/fichiers_fabricants/Bruker/tims_doc/tdf-sdk/example_data/200ngHeLaPASEF_2min_compressed.d/analysis.tdf
  //-o /tmp/test.xml


  QTextStream errorStream(stderr, QIODevice::WriteOnly);

  try
    {
      qDebug();
      QTextStream outputStream(stdout, QIODevice::WriteOnly);
      pappso::UiMonitorText monitor(outputStream);


      QCommandLineParser parser;

      // throw pappso::PappsoException("test");
      parser.setApplicationDescription(QString(SOFTWARE_NAME)
                                         .append(" ")
                                         .append(PAPPSOMSTOOLS_VERSION)
                                         .append(" mzXML converter"));
      parser.addHelpOption();
      parser.addVersionOption();
      QCommandLineOption inputOption(
        QStringList() << "i",
        QCoreApplication::translate("main", "mz data file <input>."),
        QCoreApplication::translate("main", "input"));


      QCommandLineOption fastaOption(
        QStringList() << "f",
        QCoreApplication::translate("main", "fasta file <fasta>."),
        QCoreApplication::translate("main", "fasta"));


      QCommandLineOption cpusOption(
        QStringList() << "c"
                      << "cpus",
        QCoreApplication::translate("main", "number of CPUs to use <cpus>."),
        QCoreApplication::translate("main", "cpus"));


      QCommandLineOption rtBeginOption(
        QStringList() << "rtb",
        QCoreApplication::translate("main",
                                    "retention time start in seconds <rtb>."),
        QCoreApplication::translate("main", "rtb"));

      QCommandLineOption rtEndOption(
        QStringList() << "rte",
        QCoreApplication::translate("main",
                                    "retention time end in seconds <rte>."),
        QCoreApplication::translate("main", "rte"));


      QCommandLineOption ms2filtersOption(
        QStringList() << "ms2filters",
        QCoreApplication::translate(
          "main",
          "apply filters specified in <ms2filters> (example: "
          "chargeDeconvolution|0.02dalton)."),
        QCoreApplication::translate("main", "ms2filters"));

      parser.addOption(inputOption);
      parser.addOption(fastaOption);
      parser.addOption(cpusOption);
      parser.addOption(ms2filtersOption);
      parser.addOption(rtBeginOption);
      parser.addOption(rtEndOption);

      qDebug();

      // Process the actual command line arguments given by the user
      parser.process(*app);

      // QCoreApplication * app(this);
      // Add your main code here
      qDebug();

      const QDateTime dt_begin = QDateTime::currentDateTime();
      const QStringList args   = parser.positionalArguments();


      uint cpu_number = 100;
      QString cpusStr = parser.value(cpusOption);
      if(!cpusStr.isEmpty())
        {
          cpu_number = cpusStr.toUInt();
        }

      uint ideal_number_of_thread = (uint)QThread::idealThreadCount();
      // QThreadPool::globalInstance()->setMaxThreadCount(1);
      if(cpu_number > ideal_number_of_thread)
        {
          cpu_number = ideal_number_of_thread;
        }
      else
        {
          QThreadPool::globalInstance()->setMaxThreadCount(cpu_number);
        }


      QString fastaFileStr = parser.value(fastaOption);
      ProteinCollection protein_collection;
      if(fastaFileStr.isEmpty())
        {
          throw pappso::PappsoException(
            tr("fasta file is empty, please provide the fasta file path "
               "with -f option"));
        }
      else
        {
          monitor.setTitle("reading fasta file");
          QFile fastaFile(fastaFileStr);
          pappso::FastaReader reader(protein_collection);
          reader.parse(fastaFile);
        }

      QString mzFileStr = parser.value(inputOption);
      if(mzFileStr.isEmpty())
        {
          throw pappso::PappsoException(
            tr("input file is empty, please provide complete mz data file path "
               "with -i option"));
        }


      monitor.setTitle("reading MS data file");
      pappso::MsFileAccessor file_access(mzFileStr, "runa1");
      file_access.setPreferredFileReaderType(pappso::MsDataFormat::brukerTims,
                                             pappso::FileReaderType::tims_ms2);
      file_access.getMsRunIds();

      qDebug();
      pappso::MsRunReaderSPtr p_reader;
      p_reader = file_access.msRunReaderSPtr(file_access.getMsRunIds().front());
      pappso::TimsMsRunReaderMs2 *tims2_reader =
        dynamic_cast<pappso::TimsMsRunReaderMs2 *>(p_reader.get());
      if(tims2_reader != nullptr)
        {
          qDebug();
          tims2_reader->setMs2BuiltinCentroid(true);

          std::shared_ptr<pappso::FilterSuiteString> ms2filter;

          tims2_reader->setMs2FilterCstSPtr(ms2filter);
          qDebug();
        }


      qDebug();


      monitor.setTitle("preparing spectrum list");
      pappso::MsRunQualifiedSpectrumLoader spectrum_list_reader;

      pappso::MsRunReadConfig config;
      config.setNeedPeakList(true);
      config.setMsLevels({2});
      QString rt = parser.value(rtBeginOption);
      if(!rt.isEmpty())
        {
          config.setRetentionTimeStartInSeconds(rt.toInt());
        }
      rt = parser.value(rtEndOption);
      if(!rt.isEmpty())
        {
          config.setRetentionTimeEndInSeconds(rt.toInt());
        }
      p_reader.get()->readSpectrumCollection2(config, spectrum_list_reader);
      // p_reader.get()->readSpectrumCollection2();


      pappso::AaStringCodeMassMatching aa_matching(
        protein_collection.getAaCode(),
        7,
        pappso::PrecisionFactory::getDaltonInstance(0.01));

      for(auto &qualified_spectrum :
          spectrum_list_reader.getQualifiedMassSpectrumList())
        {
          SpectrumIntegerCode coded_spectrum(qualified_spectrum);

          coded_spectrum.aminoAcidMassMatching(aa_matching);
          protein_collection.evaluateSpectrumIntegerCode(coded_spectrum);
        }


      outputStream.flush();
      qDebug();
    }
  catch(pappso::PappsoException &error)
    {
      errorStream << "Oops! an error occurred in PAPPSO MS tools. Dont Panic :"
                  << Qt::endl;
      errorStream << error.qwhat() << Qt::endl;
      app->exit(1);
      exit(1);
    }

  catch(std::exception &error)
    {
      errorStream << "Oops! an error occurred in PAPPSO MS tools. Dont Panic :"
                  << Qt::endl;
      errorStream << error.what() << Qt::endl;
      app->exit(1);
      exit(1);
    }


  // you must call quit when complete or the program will stay in the
  // messaging loop
  quit();
}

// call this routine to quit the application
void
PtSpectrumProteinMatcher::quit()
{
  // you can do some cleanup here
  // then do emit finished to signal CoreApplication to quit
  emit finished();
}

// shortly after quit is called the CoreApplication will signal this routine
// this is a good place to delete any objects that were created in the
// constructor and/or to stop any threads
void
PtSpectrumProteinMatcher::aboutToQuitApp()
{
  // stop threads
  // sleep(1);   // wait for threads to stop.
  // delete any objects
}


int
main(int argc, char **argv)
{
  // QTextStream consoleErr(stderr);
  // QTextStream consoleOut(stdout, QIODevice::WriteOnly);
  // ConsoleOut::setCout(new QTextStream(stdout, QIODevice::WriteOnly));
  // ConsoleOut::setCerr(new QTextStream(stderr, QIODevice::WriteOnly));
  qDebug();
  QCoreApplication app(argc, argv);
  qDebug();
  QCoreApplication::setApplicationName("pt-spectrumdecoder");
  QCoreApplication::setApplicationVersion(PAPPSOMSTOOLS_VERSION);
  QLocale::setDefault(QLocale::system());

  // create the main class
  PtSpectrumProteinMatcher myMain;
  // connect up the signals
  QObject::connect(&myMain, SIGNAL(finished()), &app, SLOT(quit()));
  QObject::connect(
    &app, SIGNAL(aboutToQuit()), &myMain, SLOT(aboutToQuitApp()));
  qDebug();


  // This code will start the messaging engine in QT and in
  // 10ms it will start the execution in the MainClass.run routine;
  QTimer::singleShot(10, &myMain, SLOT(run()));
  return app.exec();
}
