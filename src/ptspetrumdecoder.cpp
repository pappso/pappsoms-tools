/**
 * \file ptspectrumdecoder.h
 * \date 10/06/2023
 * \author Olivier Langella
 * \brief Find amino acid candidates from spectrum
 */

/*******************************************************************************
 * Copyright (c) 2023 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#include "ptspetrumdecoder.h"

#include <QDebug>
#include <QCommandLineParser>
#include <QDateTime>
#include <QTimer>
#include <QFile>
#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/msfile/msfileaccessor.h>
#include <pappsomspp/msrun/private/timsmsrunreaderms2.h>
#include <pappsomspp/processing/uimonitor/uimonitorvoid.h>
#include <pappsomspp/processing/uimonitor/uimonitortextpercent.h>
#include <pappsomspp/processing/filters/filtersuitestring.h>
#include <pappsomspp/processing/filters/filterpeakdelta.h>
#include <pappsomspp/amino_acid/aastringcodemassmatching.h>
#include <pappsomspp/msfile/mzformatenumstr.h>
#include <pappsomspp/amino_acid/aacode.h>
#include <pappsomspp/amino_acid/aamodification.h>
#include <pappsomspp/processing/filters/filterresample.h>
#include <pappsomspp/processing/filters/filterpass.h>

#include <pappsomspp/processing/filters/filterchargedeconvolution.h>
#include <QFileInfo>
#include <QThreadPool>


//./src/pt-spetrumdecoder -i
/// gorgone/pappso/data_extraction_pappso/mzML/20120906_balliau_extract_1_A10_urzb-3.mzML
//-s 15081
// AAPGVDLTQLLNNMR

PtSpetrumDecoder::PtSpetrumDecoder(QObject *parent [[maybe_unused]])
{

  // get the instance of the main application
  app = QCoreApplication::instance();
  // setup everything here
  // create any global objects
  // setup debug and warning mode
}

void
PtSpetrumDecoder::run()
{
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));
  /* ./src/pt-mzxmlconverter -m --ms2filters "chargeDeconvolution|0.02dalton" -i
   /gorgone/pappso/fichiers_fabricants/Bruker/tims_doc/tdf-sdk/example_data/200ngHeLaPASEF_2min_compressed.d/analysis.tdf
   -o
   /gorgone/pappso/versions_logiciels_pappso/xtpcpp/bruker/200ngHeLaPASEF_2min.mzXML
  */

  //./src/pt-mzxmlconverter -i
  /// gorgone/pappso/fichiers_fabricants/Bruker/tims_doc/tdf-sdk/example_data/200ngHeLaPASEF_2min_compressed.d/analysis.tdf
  //-o /tmp/test.xml


  QTextStream errorStream(stderr, QIODevice::WriteOnly);

  try
    {
      qDebug();
      QCommandLineParser parser;

      // throw pappso::PappsoException("test");
      parser.setApplicationDescription(QString(SOFTWARE_NAME)
                                         .append(" ")
                                         .append(PAPPSOMSTOOLS_VERSION)
                                         .append(" mzXML converter"));
      parser.addHelpOption();
      parser.addVersionOption();
      QCommandLineOption inputOption(
        QStringList() << "i",
        QCoreApplication::translate("main", "mz data file <input>."),
        QCoreApplication::translate("main", "input"));


      QCommandLineOption indexOption(
        QStringList() << "s",
        QCoreApplication::translate("main", "spectrum index <spectrum_index>."),
        QCoreApplication::translate("main", "spectrum_index"));


      QCommandLineOption cpusOption(
        QStringList() << "c"
                      << "cpus",
        QCoreApplication::translate("main", "number of CPUs to use <cpus>."),
        QCoreApplication::translate("main", "cpus"));


      QCommandLineOption ms2filtersOption(
        QStringList() << "ms2filters",
        QCoreApplication::translate(
          "main",
          "apply filters specified in <ms2filters> (example: "
          "chargeDeconvolution|0.02dalton)."),
        QCoreApplication::translate("main", "ms2filters"));

      parser.addOption(inputOption);
      parser.addOption(indexOption);
      parser.addOption(cpusOption);
      parser.addOption(ms2filtersOption);

      qDebug();

      // Process the actual command line arguments given by the user
      parser.process(*app);

      // QCoreApplication * app(this);
      // Add your main code here
      qDebug();

      const QDateTime dt_begin = QDateTime::currentDateTime();
      const QStringList args   = parser.positionalArguments();


      uint cpu_number = 100;
      QString cpusStr = parser.value(cpusOption);
      if(!cpusStr.isEmpty())
        {
          cpu_number = cpusStr.toUInt();
        }

      uint ideal_number_of_thread = (uint)QThread::idealThreadCount();
      // QThreadPool::globalInstance()->setMaxThreadCount(1);
      if(cpu_number > ideal_number_of_thread)
        {
          cpu_number = ideal_number_of_thread;
        }
      else
        {
          QThreadPool::globalInstance()->setMaxThreadCount(cpu_number);
        }


      QString mzFileStr = parser.value(inputOption);
      if(mzFileStr.isEmpty())
        {
          throw pappso::PappsoException(
            tr("input file is empty, please provide complete mz data file path "
               "with -i option"));
        }

      std::size_t spectrum_index = parser.value(indexOption).toULongLong();

      pappso::MsFileAccessor file_access(mzFileStr, "runa1");
      file_access.setPreferredFileReaderType(pappso::MsDataFormat::brukerTims,
                                             pappso::FileReaderType::tims_ms2);
      file_access.getMsRunIds();

      qDebug();
      pappso::MsRunReaderSPtr p_reader;
      p_reader = file_access.msRunReaderSPtr(file_access.getMsRunIds().front());
      pappso::TimsMsRunReaderMs2 *tims2_reader =
        dynamic_cast<pappso::TimsMsRunReaderMs2 *>(p_reader.get());
      if(tims2_reader != nullptr)
        {
          qDebug();
          tims2_reader->setMs2BuiltinCentroid(true);

          std::shared_ptr<pappso::FilterSuiteString> ms2filter;

          tims2_reader->setMs2FilterCstSPtr(ms2filter);
          qDebug();
        }


      qDebug();
      QTextStream outputStream(stdout, QIODevice::WriteOnly);

      pappso::MassSpectrum spectrum_simple =
        *(p_reader->massSpectrumSPtr(spectrum_index).get());


      pappso::FilterResampleKeepGreater(160).filter(spectrum_simple);
      pappso::FilterChargeDeconvolution(
        pappso::PrecisionFactory::getDaltonInstance(0.02))
        .filter(spectrum_simple);
      //.applyCutOff(150).takeNmostIntense(100).applyDynamicRange(100);
      pappso::FilterGreatestY(100).filter(spectrum_simple);

      pappso::FilterPeakDelta filter_peak_delta;
      filter_peak_delta.filter(spectrum_simple);

      pappso::FilterGreatestY(200).filter(spectrum_simple);


      pappso::AaCode aa_code;
      aa_code.addAaModification(
        'C', pappso::AaModification::getInstance("MOD:00397"));
      pappso::AaStringCodeMassMatching aaMatching(
        aa_code, 7, pappso::PrecisionFactory::getDaltonInstance(0.01));


      std::vector<double> mass_list = spectrum_simple.xValues();
      std::vector<uint32_t> code_list =
        aaMatching.getAaCodeFromMassList(mass_list);


      code_list = aaMatching.filterCodeList(code_list);


      outputStream << ">" << p_reader->getMsRunId()->getSampleName() << "#"
                   << spectrum_index << "\n";

      pappso::AaStringCodec aa_codec(aa_code);
      for(auto code : code_list)
        {
          outputStream << aa_codec.decode(code) << "\n";
        }

      outputStream.flush();
      qDebug();
    }
  catch(pappso::PappsoException &error)
    {
      errorStream << "Oops! an error occurred in PAPPSO MS tools. Dont Panic :"
                  << Qt::endl;
      errorStream << error.qwhat() << Qt::endl;
      app->exit(1);
      exit(1);
    }

  catch(std::exception &error)
    {
      errorStream << "Oops! an error occurred in PAPPSO MS tools. Dont Panic :"
                  << Qt::endl;
      errorStream << error.what() << Qt::endl;
      app->exit(1);
      exit(1);
    }


  // you must call quit when complete or the program will stay in the
  // messaging loop
  quit();
}

// call this routine to quit the application
void
PtSpetrumDecoder::quit()
{
  // you can do some cleanup here
  // then do emit finished to signal CoreApplication to quit
  emit finished();
}

// shortly after quit is called the CoreApplication will signal this routine
// this is a good place to delete any objects that were created in the
// constructor and/or to stop any threads
void
PtSpetrumDecoder::aboutToQuitApp()
{
  // stop threads
  // sleep(1);   // wait for threads to stop.
  // delete any objects
}


int
main(int argc, char **argv)
{
  // QTextStream consoleErr(stderr);
  // QTextStream consoleOut(stdout, QIODevice::WriteOnly);
  // ConsoleOut::setCout(new QTextStream(stdout, QIODevice::WriteOnly));
  // ConsoleOut::setCerr(new QTextStream(stderr, QIODevice::WriteOnly));
  qDebug();
  QCoreApplication app(argc, argv);
  qDebug();
  QCoreApplication::setApplicationName("pt-spectrumdecoder");
  QCoreApplication::setApplicationVersion(PAPPSOMSTOOLS_VERSION);
  QLocale::setDefault(QLocale::system());

  // create the main class
  PtSpetrumDecoder myMain;
  // connect up the signals
  QObject::connect(&myMain, SIGNAL(finished()), &app, SLOT(quit()));
  QObject::connect(
    &app, SIGNAL(aboutToQuit()), &myMain, SLOT(aboutToQuitApp()));
  qDebug();


  // This code will start the messaging engine in QT and in
  // 10ms it will start the execution in the MainClass.run routine;
  QTimer::singleShot(10, &myMain, SLOT(run()));
  return app.exec();
}
