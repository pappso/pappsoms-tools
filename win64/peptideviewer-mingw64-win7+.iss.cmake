[Setup]
AppName=Peptide Viewer

; Set version number below
#define public version "${PAPPSOMSTOOLS_VERSION}"
AppVersion={#version}

#define public arch "mingw64"
#define public platform "win7+"
#define sourceDir "C:\msys64\home\polipo\devel\pappsoms-tools"
#define cmake_build_dir "C:\msys64\home\polipo\devel\pappsoms-tools\build"

; Set version number below
AppVerName=Peptide viewer version {#version}
DefaultDirName={pf}\peptideviewer
DefaultGroupName=peptideviewer
OutputDir="C:\msys64\home\polipo\devel\pappsoms-tools\win64"

; Set version number below
OutputBaseFilename=peptideviewer-{#arch}-{#platform}-v{#version}-setup

; Set version number below
OutputManifestFile=peptideviewer-{#arch}-{#platform}-v{#version}-setup-manifest.txt
ArchitecturesAllowed=x64
ArchitecturesInstallIn64BitMode=x64

LicenseFile="{#sourceDir}\COPYING"
AppCopyright="Copyright (C) 2019- Olivier Langella"

AllowNoIcons=yes
AlwaysShowComponentsList=yes
AllowRootDirectory=no
AllowCancelDuringInstall=yes
AppComments="PeptideViewer, by Olivier Langella"
AppContact="Olivier Langella, research engineer at CNRS, France"
CloseApplications=yes
CreateUninstallRegKey=yes
DirExistsWarning=yes
WindowResizable=yes
; WizardImageFile="{#sourceDir}\images\splashscreen-innosetup.bmp"
WizardImageStretch=yes

[Dirs]
Name: "{app}"

[Files]
Source: "C:\xtpcpp-libdeps\*"; DestDir: {app}; Flags: ignoreversion recursesubdirs;
Source: "C:\msys64\home\polipo\devel\libodsstream\build\src\libodsstream-qt5.so"; DestDir: {app}; Flags: ignoreversion recursesubdirs;
Source: "C:\msys64\home\polipo\devel\pappsomspp\build\src\libpappsomspp-qt5.so"; DestDir: {app}; Flags: ignoreversion recursesubdirs;
Source: "C:\msys64\home\polipo\devel\pappsomspp\build\src\pappsomspp\widget\libpappsomspp-widget-qt5.so"; DestDir: {app}; Flags: ignoreversion recursesubdirs;

Source: "{#sourceDir}\COPYING"; DestDir: {app}; Flags: isreadme; Components: peptideviewerComp
Source: "{#sourceDir}\src\resources\pappsoms-tools.svg"; DestDir: {app}; Components: peptideviewerComp
; Source: "{#sourceDir}\win64\xtandempipeline_icon.ico"; DestDir: {app}; Components: peptideviewerComp

Source: "{#cmake_build_dir}\src\gui\pt-peptideviewer.exe"; DestDir: {app}; Components: peptideviewerComp 

[Icons]
Name: "{group}\peptideviewer"; Filename: "{app}\pt-peptideviewer.exe"; WorkingDir: "{app}"
Name: "{group}\Uninstall peptideviewer"; Filename: "{uninstallexe}"

[Types]
Name: "peptideviewerType"; Description: "Full installation"

[Components]
Name: "peptideviewerComp"; Description: "PeptideViewer files"; Types: peptideviewerType

[Run]
;Filename: "{app}\README"; Description: "View the README file"; Flags: postinstall shellexec skipifsilent
Filename: "{app}\pt-peptideviewer.exe"; Description: "Launch Peptide Viewer"; Flags: postinstall nowait unchecked

