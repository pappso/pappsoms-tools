[Setup]
AppName=Peptide Viewer

#define public winerootdir "z:"
; Set version number below
#define public version "${PAPPSOMSTOOLS_VERSION}"
AppVersion={#version}

#define public arch "mingw64"
#define public platform "win7+"
#define sourceDir "z:/home/langella/developpement/git/pappsoms-tools/"
#define cmake_build_dir "z:/home/langella/developpement/git/pappsoms-tools/wbuild"

; Set version number below
AppVerName=Peptide viewer version {#version}
DefaultDirName={pf}\peptideviewer
DefaultGroupName=peptideviewer
OutputDir="{#cmake_build_dir}"

; Set version number below
OutputBaseFilename=peptideviewer-{#arch}-{#platform}-v{#version}-setup

; Set version number below
OutputManifestFile=peptideviewer-{#arch}-{#platform}-v{#version}-setup-manifest.txt
ArchitecturesAllowed=x64
ArchitecturesInstallIn64BitMode=x64

LicenseFile="{#sourceDir}\COPYING"
AppCopyright="Copyright (C) 2019- Olivier Langella"

AllowNoIcons=yes
AlwaysShowComponentsList=yes
AllowRootDirectory=no
AllowCancelDuringInstall=yes
AppComments="PeptideViewer, by Olivier Langella"
AppContact="Olivier Langella, research engineer at CNRS, France"
CloseApplications=yes
CreateUninstallRegKey=yes
DirExistsWarning=yes
WindowResizable=yes
; WizardImageFile="{#sourceDir}\images\splashscreen-innosetup.bmp"
WizardImageStretch=yes

[Dirs]
Name: "{app}"


[Files]
Source: "z:/win64/mxeqt6_dll/*"; DestDir: {app}; Flags: ignoreversion recursesubdirs;
Source: "z:/backup2/mxeqt6/usr/x86_64-w64-mingw32.shared/qt6/plugins/*"; DestDir: {app}/plugins; Flags: ignoreversion recursesubdirs
Source: "z:/home/langella/developpement/git/cutelee/wbuild/templates/lib/libCuteleeQt6Templates.dll"; DestDir: {app}; Flags: ignoreversion recursesubdirs;
Source: "z:/home/langella/developpement/git/cutelee/wbuild/cutelee-qt6/6.1/*"; DestDir: {app}/cutelee-qt6/6.1; Flags: ignoreversion recursesubdirs;
Source: "z:/home/langella/developpement/git/libpwizlite/wbuild/src/libpwizlite.dll"; DestDir: {app}; Flags: ignoreversion recursesubdirs;
Source: "z:/home/langella/developpement/git/libodsstream/wbuild/src/libodsstream.dll"; DestDir: {app}; Flags: ignoreversion recursesubdirs;
Source: "z:/home/langella/developpement/git/pappsomspp/wbuild/src/libpappsomspp.dll"; DestDir: {app}; Flags: ignoreversion recursesubdirs;
Source: "z:/home/langella/developpement/git/pappsomspp/wbuild/src/pappsomspp/widget/libpappsomspp-widget.dll"; DestDir: {app}; Flags: ignoreversion recursesubdirs;

Source: "{#sourceDir}\COPYING"; DestDir: {app}; Flags: isreadme; Components: peptideviewerComp
Source: "{#sourceDir}\src\resources\pappsoms-tools.svg"; DestDir: {app}; Components: peptideviewerComp

Source: "{#cmake_build_dir}\src\gui\pt-peptideviewer.exe"; DestDir: {app}; Components: peptideviewerComp 
Source: "{#cmake_build_dir}\src\tandemwrapper.exe"; DestDir: {app}; Components: peptideviewerComp
Source: "{#cmake_build_dir}\src\pt-mzxmlconverter.exe"; DestDir: {app}; Components: peptideviewerComp


[Icons]
Name: "{group}\peptideviewer"; Filename: "{app}\pt-peptideviewer.exe"; WorkingDir: "{app}"
Name: "{group}\Uninstall peptideviewer"; Filename: "{uninstallexe}"

[Types]
Name: "peptideviewerType"; Description: "Full installation"

[Components]
Name: "peptideviewerComp"; Description: "PeptideViewer files"; Types: peptideviewerType

[Run]
;Filename: "{app}\README"; Description: "View the README file"; Flags: postinstall shellexec skipifsilent
Filename: "{app}\pt-peptideviewer.exe"; Description: "Launch Peptide Viewer"; Flags: postinstall nowait unchecked
